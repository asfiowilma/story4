from django.test import SimpleTestCase, TestCase
from django.urls import reverse, resolve
from story5.views import matkul, details, addMatkul, delMatkul

class TestUrls(TestCase):

    def test_matkul_resolves(self):
        url = reverse('matkul')
        self.assertEquals(resolve(url).func, matkul)

    def test_details_resolves(self):
        url = reverse('details', args=['1'])
        self.assertEquals(resolve(url).func, details)

    def test_add_resolves(self):
        url = reverse('add')
        self.assertEquals(resolve(url).func, addMatkul)

    def test_del_resolves(self):
        url = reverse('del', args=['1'])
        self.assertEquals(resolve(url).func, delMatkul)