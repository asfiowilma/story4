from django.test import TestCase, Client
from django.urls import reverse
from story5.models import Matkul, Tugas

class TestViews(TestCase):

    def setUp(self): 
        self.client = Client()
        self.matkul = reverse('matkul')

    def test_Matkul_GET(self): 
        response = self.client.get(self.matkul)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'story5/matkul.html')
