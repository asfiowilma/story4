from django.forms import ModelForm
from .models import Matkul

class Form(ModelForm): 
    class Meta: 
        model = Matkul
        fields = '__all__'