from django.db import models
from django.utils import timezone
from datetime import date

# Create your models here.
class Matkul(models.Model):
    nama = models.CharField(max_length=50)
    dosen = models.CharField(max_length=50)
    sks = models.IntegerField()
    desc = models.TextField()
    sem = models.CharField(max_length=20)
    ruang = models.CharField(max_length=20)

    def __str__ (self):
        return self.nama

class Tugas(models.Model): 
    nama = models.CharField(max_length=50)
    dl = models.DateTimeField()
    # dl = models.DateField()
    # status = due()

    matkul = models.ForeignKey(Matkul, default=1, on_delete=models.CASCADE)
    
    def due(self): 
        return date.today() > self.dl

    def __str__ (self):
        return self.nama

    class Meta:
        ordering = ["dl"]