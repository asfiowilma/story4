[![pipeline status](https://gitlab.com/asfiowilma/story4/badges/master/pipeline.svg)](https://gitlab.com/asfiowilma/story4/-/commits/master)

Repository for Web Design and Programming Stories 1 through 6 
*  Story1 - Profile Page. A basic web-page that displays profile info. ([view](http://story7-pacilboard.herokuapp.com/))
*  Story2 - Persona, Wireframe, Prototype.
*  Story3 - Realizing the Prototype. 
*  Story4 - Personal Website. Implementation of the previous two stories, now deployed. ([view](https://ppw-story-litha.herokuapp.com/))
*  Story5 - Mata Kuliah. Django app that allows user to add and delete subjects, and add assignments to each subjects. ([view](https://ppw-story-litha.herokuapp.com/matkul/))
*  Story6 - Kegiatan Organizer. Django app that allows user to add and delete events and participants, and displays them in a nice masonry layout. ([view](https://ppw-story-litha.herokuapp.com/kegiatan/))