from django.db import models
from django.urls import reverse

# Create your models here.

class Peserta(models.Model):
    nama = models.CharField(max_length=20, unique=True)
    
    def __str__(self):
        return self.nama


    def get_absolute_url(self):
        urlnama = self.nama.replace(" ", "-")
        return reverse('story6:detailPeserta', args=[str(urlnama)])

class Kegiatan(models.Model):
    nama = models.CharField(max_length=20, unique=True)
    peserta = models.ManyToManyField(Peserta, blank=True)

    def __str__(self):
        return self.nama

    def get_absolute_url(self):
        urlnama = self.nama.replace(" ", "-")
        return reverse('story6:detail', args=[str(urlnama)])
