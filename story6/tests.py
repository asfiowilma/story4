from django.test import TestCase, Client
from django.urls import reverse, resolve

from .models import Kegiatan, Peserta

# Create your tests here.
class TestUrls (TestCase):
    def setUp(self):
        self.kegiatantest = Kegiatan.objects.create(
            nama = "kegiatan"
        )
        self.pesertatest = Peserta.objects.create(
            nama = "peserta"
        )
        self.client = Client()
        self.indexurl = reverse('story6:index')
        self.detailurl = reverse('story6:detail', args=['kegiatan'])
        self.addpesertaurl = reverse('story6:addPeserta',  args=['1', 'index'])
        self.detailpesertaurl = reverse('story6:detailPeserta', args=['peserta'])
        self.addkegiatanurl = reverse('story6:addKegiatan',  args=['1', 'index'])

    def test_index_GET(self):
        response = self.client.get(self.indexurl)
        self.assertContains(response, 'Kegiatan', status_code=200)

    def test_detail_GET(self):
        response = self.client.get(self.detailurl)
        self.assertContains(response, 'Detail', status_code=200)

    def test_detailPeserta_GET(self):
        response = self.client.get(self.detailpesertaurl)
        self.assertContains(response, 'Detail', status_code=200)

class TestModels(TestCase): 
    def setUp(self):
        self.kegiatantest = Kegiatan.objects.create(
            nama = "lagi ngetes"
        )
        self.pesertatest = Peserta.objects.create(
            nama = "peserta nomor satu"
        )
        self.rekreasi = Kegiatan.objects.create(nama="rekreasi")
        self.bbw = Kegiatan.objects.create(nama="bbw")
        self.jalan = Kegiatan.objects.create(nama="jalan jalan")

        self.dodo = Peserta.objects.create(nama="dodo")
        self.aldi = Peserta.objects.create(nama="aldi")
        self.raihan = Peserta.objects.create(nama="raihan")
        self.mustafa = Peserta.objects.create(nama="mustafa")
        self.adimas = Peserta.objects.create(nama="adimas")
        self.ican = Peserta.objects.create(nama="ican")

        self.kegiatantest.peserta.add(self.pesertatest)
        self.rekreasi.peserta.add(self.dodo, self.aldi, self.raihan, self.mustafa)
        self.bbw.peserta.add(self.aldi, self.raihan, self.ican)
        self.jalan.peserta.add(self.aldi, self.adimas, self.raihan, self.mustafa)

    def test_nama_kegiatan(self):
        self.assertEquals(str(self.kegiatantest), "lagi ngetes")

    def test_nama_peserta(self):
        self.assertEquals(str(self.pesertatest), "peserta nomor satu")

    def test_absolute_url_peserta(self):
        self.assertEquals(self.pesertatest.get_absolute_url(), "/kegiatan/detailPeserta/peserta-nomor-satu")

    def test_absolute_url_kegiatan(self):
        self.assertEquals(self.kegiatantest.get_absolute_url(), "/kegiatan/detail/lagi-ngetes")
