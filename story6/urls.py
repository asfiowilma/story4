from django.urls import path
from . import views

app_name = "story6"

urlpatterns = [
    path('', views.index, name='index'),
    path('detail/<str:nama>', views.detail, name='detail'),
    path('addPeserta/<int:id>/<str:origin>', views.addPeserta, name='addPeserta'),
    path('delPeserta/<int:id>/<int:pk>/<str:origin>', views.delPeserta, name='delPeserta'),
    path('detailPeserta/<str:nama>', views.detailPeserta, name='detailPeserta'),
    path('addKegiatan/<int:id>/<str:origin>', views.addKegiatan, name='addKegiatan')
]